import 'package:flutter/material.dart';

class SubTitle extends StatelessWidget {
  const SubTitle({
    this.subTitle,
    this.price,
    this.category,
    this.label,
    this.color,
  });

  final String subTitle;
  final String price;
  final String category;
  final String label;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 480.0,
      height: 75.0,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: ListTile(
        leading: Container(
          width: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Text(
            '\$$price',
            style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
        title: Text(
          subTitle,
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
        subtitle: Text(
          category,
          textAlign: TextAlign.right,
          style: TextStyle(
            color: Colors.white,
            fontSize: 10.0,
          ),
        ),
        trailing: Container(
          width: 50.0,
          height: 50.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Image.network(
            label,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
