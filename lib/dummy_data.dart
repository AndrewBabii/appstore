import 'package:appstore/const.dart';
import 'package:appstore/item.dart';

class DummyData {
  List<Item> dummyData = [
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '4.9',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '15.9',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '4.19',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '2.99',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '4',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '4.99',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '16',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '4.99',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
    Item(
      body: kBody,
      mainImage:
          'https://i.pinimg.com/originals/64/a2/8c/64a28c25e19874be05e7c1926340f999.jpg',
      mainTitle: 'GAME OF THE DAY',
      price: '5.99',
      subTitle: 'Mortal Kombat',
      category: 'Fighting',
      label:
          'https://i.pinimg.com/originals/3b/b7/c8/3bb7c8557ff0a35541b63e57397a880e.png',
      bodyImage: 'https://www.meme-arsenal.com/memes/fda8aa85acba3a6d860eed9a8912a20d.jpg',
    ),
  ];
}
