import 'package:appstore/item.dart';
import 'package:appstore/main.dart';
import 'package:appstore/subtitle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LabelImage extends StatelessWidget {
  const LabelImage({
    Key key,
    this.item /*, this.image, this.onTap, this.title,*/,
    /* this.onTap,*/
  }) : super(key: key);

  final Item item;

  void gotoMainScreen(BuildContext ctx) {
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return MyHomePage();
    }));
  }

/*  final VoidCallback onTap;*/


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Image.network(
              item.mainImage,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            bottom: 80.0,
            right: 10.0,
            child: SizedBox(
              width: 120.0,
              child: Text(
                item.mainTitle,
                // textHeightBehavior: ,
                textAlign: TextAlign.right,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                  height: 0.8,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            width: 300.0,
            child: SubTitle(
              price: item.price,
              subTitle: item.subTitle,
              label: item.label,
              category: item.category,
            ),
          ),
          /*Positioned(
            top: 15.0,
            left: 15.0,
            child: InkWell(
              onTap: (){gotoMainScreen(context);},
              child: Icon(
                CupertinoIcons.clear_thick_circled,
                size: 25.0,
              ),
            ),
          ),*/
        ],
      ),
      // ),
    );
  }
}
