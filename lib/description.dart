import 'package:appstore/const.dart';
import 'package:appstore/item.dart';
import 'package:appstore/label_image.dart';
import 'package:appstore/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Description extends StatelessWidget {
  Description(
    this.item,
    /*this.onTap*/
  );

/*  final VoidCallback onTap;*/
  final Item item;

  void transition(BuildContext context) {
    print('ontap fab');
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return MyHomePage();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Material(
          child: Hero(

            tag: heroTag,
            child: Stack(
              children: [
                ListView(
                  children: [
                    LabelImage(
                      item: item,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(item.body),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          heroTag: 'fab',
          onPressed: () {
            transition(context);
          },
          child: Icon(
            CupertinoIcons.clear_thick_circled,
            color: Colors.black54,
          ),
          elevation: 0.0,
          // clipBehavior: Clip.hardEdge,
          backgroundColor: Color.fromRGBO(0, 0, 0, 0.0),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
      ),
    );
  }
}
