class Item {
  Item({
    this.subTitle,
    this.price,
    this.category,
    this.label,
    this.mainImage,
    this.mainTitle,
    this.body,
    this.bodyImage,
  });

  final String mainImage;
  final String mainTitle;

  final String subTitle;
  final String price;
  final String category;
  final String label;
  final String body;
  final String bodyImage;
}
