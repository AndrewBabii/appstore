import 'package:appstore/description.dart';
import 'package:appstore/dummy_data.dart';
import 'package:appstore/item.dart';
import 'package:appstore/label_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;
  DummyData dd = DummyData();

  void transition(BuildContext context, Item item) {
    print('ontap');
    Navigator.of(context).push(MaterialPageRoute<void>(
      maintainState: ,
      builder: (BuildContext context) {
        return Description(item );
        },
    ),
    );
  }

  /*void transition(BuildContext context, Item item) {
    print('ontap');
    Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (BuildContext context) {
        return Description(item *//*onTap*//*);
      },
    ),
    );
  }*/

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    List<Item> items = dd.dummyData;
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (context, ind) {
          return InkWell(
            onTap: () {
            print('under ontap');
              transition(context, items[ind]);
            },
            child: LabelImage(
              item: items[ind],
            ),
          );
        },
        itemCount: items.length,
      ),
      bottomNavigationBar: CupertinoTabBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              CupertinoIcons.search,
              color: Colors.grey,
            ),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              CupertinoIcons.app,
              color: Colors.grey,
            ),
            label: 'Arcade',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              CupertinoIcons.table_badge_more,
              color: Colors.grey,
            ),
            label: 'Apps',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.directions_car,
              color: Colors.grey,
            ),
            label: 'Games',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              CupertinoIcons.today,
              color: Colors.blue,
            ),
            label: 'Today',
          ),
        ],
        currentIndex: 4,

      ),
    );
  }
}
